const { publishSupplyWarningNotifications } = require("./DomainNotifications");
const axios = require("axios");
const xsenv = require("@sap/xsenv");
xsenv.loadEnv();
const xsuaa = xsenv.serviceCredentials({ label: "xsuaa" });
const destination = xsenv.serviceCredentials({ label: "destination" });

const categories = {
  2: "Condiments",
  4: "Dairy Products",
  6: "Meat/Poultry"
};

(async () => {
  try {
    const res = await axios("https://services.odata.org/V2/Northwind/Northwind.svc/Products?$format=json");

    const criticalSupplyOfCategory6 = res.data.d.results.filter(
      a => a.UnitsInStock <= a.ReorderLevel && a.CategoryID === 6
    );

    await Promise.all(
      criticalSupplyOfCategory6.map(product =>
        publishSupplyWarningNotifications({
          product: product.ProductName,
          category: categories[product.CategoryID],
          stock: `${product.UnitsInStock}`,
          recipients: ["freebomh@naver.com"]
        })
      )
    );

    console.log("Success");
  } catch (e) {
    if (e.response) {
      console.error(
        `${e.response.statusText} (${e.response.status}): ${JSON.stringify(e.response.data.error.message)}.`
      );
    } else {
      console.error(e);
    }
  }
})();
