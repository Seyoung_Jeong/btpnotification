const { NotificationService } = require("./util/NotificationAPI");

const NOTIF_TYPE_KEY = "SupplyWarning18";
const NOTIF_TYPE_VERSION = "2.0";

function createNotificationType() {
  return {
    NotificationTypeKey: NOTIF_TYPE_KEY,
    NotificationTypeVersion: NOTIF_TYPE_VERSION,
    Templates: [
      {
        Language: "ko",
        TemplateSensitive: "Low {{product}} supply ({{stock}} items left)",
        TemplatePublic: "Ciritical product supply detected",
        TemplateGrouped: "Limited Product Supply of {{category}}",
        TemplateLanguage: "Mustache",
        Subtitle: "{{product}} needs to be reordered"
      }
    ],
    Actions: [
      {
        ActionId: "AcceptLRActionKey",
        Language: "ko",
        ActionText: "Accept",
        GroupActionText: "Accept All",
        Nature: "POSITIVE"
      },
      {
        ActionId: "RejectLRActionKey",
        Language: "ko",
        ActionText: "Reject",
        GroupActionText: "Reject All",
        Nature: "NEGATIVE"
      }
    ]
  };
}

function createNotification({ product, category, stock, recipients }) {
  return {
    OriginId: "supply-warn-backend",
    NotificationTypeKey: NOTIF_TYPE_KEY,
    NotificationTypeVersion: NOTIF_TYPE_VERSION,
    NavigationTargetAction: "display",
    NavigationTargetObject: "Object",
    Priority: "High",
    ProviderId: "",
    ActorId: "",
    ActorType: "",
    ActorDisplayText: "",
    ActorImageURL: "",
    Properties: [
      {
        Key: "product",
        Language: "ko",
        Value: product,
        Type: "String",
        IsSensitive: false
      },
      {
        Key: "category",
        Language: "ko",
        Value: category,
        Type: "String",
        IsSensitive: false
      },
      {
        Key: "stock",
        Language: "ko",
        Value: stock,
        Type: "String",
        IsSensitive: false
      }
    ],
    Recipients: recipients.map(recipient => ({ RecipientId: recipient }))
  };
}

async function publishSupplyWarningNotifications(notification) {
  const notifTypes = await NotificationService.getNotificationTypes();
  const notifType = notifTypes.find(
    nType => nType.NotificationTypeKey === NOTIF_TYPE_KEY && nType.NotificationTypeVersion === NOTIF_TYPE_VERSION
  );
  if (!notifType) {
    console.log(
      `Notification Type of key ${NOTIF_TYPE_KEY} and version ${NOTIF_TYPE_VERSION} was not found. Creating it...`
    );
    await NotificationService.postNotificationType(createNotificationType());
  }
  return await NotificationService.postNotification(createNotification(notification));
}

module.exports = { publishSupplyWarningNotifications };
