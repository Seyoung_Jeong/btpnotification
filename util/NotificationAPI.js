const { getDestination, executeHttpRequest, buildCsrfHeaders } = require("@sap-cloud-sdk/core");
const { setLogLevel } = require("@sap-cloud-sdk/util");
const { default: axios } = require("axios");
setLogLevel("error", "env-destination-accessor");
setLogLevel("error", "destination-accessor-vcap");
setLogLevel("error", "destination-accessor-service");
setLogLevel("error", "xsuaa-service");
setLogLevel("error", "proxy-util");
setLogLevel("error", "http-client");
setLogLevel("error", "environment-accessor");

const destinationName = "SAP_Notifications";
const notificationEndpoint = "v2/Notification.svc";
const notificationTypesEndpoint = "v2/NotificationType.svc";
const BearerToken =
  "eyJhbGciOiJSUzI1NiIsImprdSI6Imh0dHBzOi8vMDUwZjE1ZjJ0cmlhbC5hdXRoZW50aWNhdGlvbi51czEwLmhhbmEub25kZW1hbmQuY29tL3Rva2VuX2tleXMiLCJraWQiOiJkZWZhdWx0LWp3dC1rZXktMTUyNDcxMDg0MiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyNmU5MGUwMTQxNjQ0YzM5YTY2MTlkMTE0ODFhMjM0MyIsImV4dF9hdHRyIjp7ImVuaGFuY2VyIjoiWFNVQUEiLCJzdWJhY2NvdW50aWQiOiI2NDk4NWUxMC1mNjhkLTRjMDEtYmZiNS01ZjhmZGY0ZGFiN2EiLCJ6ZG4iOiIwNTBmMTVmMnRyaWFsIiwic2VydmljZWluc3RhbmNlaWQiOiJkYThjMmIyZS04YTM0LTQ3MDctYjg2My03ZDhkYzI3ZTI2Y2EifSwic3ViIjoic2ItZGE4YzJiMmUtOGEzNC00NzA3LWI4NjMtN2Q4ZGMyN2UyNmNhIWIyOTc3M3x4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MSIsImF1dGhvcml0aWVzIjpbInhmc3J0LXNlcnZpY2UtYnJva2VyIWIzMDkxLkFQSUZ1bGxBY2Nlc3MiLCJ4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MS5PRFBNYW5hZ2UiLCJ1YWEucmVzb3VyY2UiLCJ4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MS5PRFBBUElBY2Nlc3MiLCJ4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MS5BUElNYW5hZ2UiLCJ4ZnNydC1hcHBsaWNhdGlvbiF0MzA5MS5DYWxsYmFjayJdLCJzY29wZSI6WyJ1YWEucmVzb3VyY2UiLCJ4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MS5PRFBBUElBY2Nlc3MiLCJ4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MS5BUElNYW5hZ2UiLCJ4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MS5BUElGdWxsQWNjZXNzIiwieGZzcnQtc2VydmljZS1icm9rZXIhYjMwOTEuT0RQTWFuYWdlIiwieGZzcnQtYXBwbGljYXRpb24hdDMwOTEuQ2FsbGJhY2siXSwiY2xpZW50X2lkIjoic2ItZGE4YzJiMmUtOGEzNC00NzA3LWI4NjMtN2Q4ZGMyN2UyNmNhIWIyOTc3M3x4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MSIsImNpZCI6InNiLWRhOGMyYjJlLThhMzQtNDcwNy1iODYzLTdkOGRjMjdlMjZjYSFiMjk3NzN8eGZzcnQtc2VydmljZS1icm9rZXIhYjMwOTEiLCJhenAiOiJzYi1kYThjMmIyZS04YTM0LTQ3MDctYjg2My03ZDhkYzI3ZTI2Y2EhYjI5NzczfHhmc3J0LXNlcnZpY2UtYnJva2VyIWIzMDkxIiwiZ3JhbnRfdHlwZSI6ImNsaWVudF9jcmVkZW50aWFscyIsInJldl9zaWciOiIzNDY2ODVlYiIsImlhdCI6MTYyNjE0MTczMiwiZXhwIjoxNjI2MTg0OTMyLCJpc3MiOiJodHRwczovLzA1MGYxNWYydHJpYWwuYXV0aGVudGljYXRpb24udXMxMC5oYW5hLm9uZGVtYW5kLmNvbS9vYXV0aC90b2tlbiIsInppZCI6IjY0OTg1ZTEwLWY2OGQtNGMwMS1iZmI1LTVmOGZkZjRkYWI3YSIsImF1ZCI6WyJ4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MSIsInhmc3J0LWFwcGxpY2F0aW9uIXQzMDkxIiwic2ItZGE4YzJiMmUtOGEzNC00NzA3LWI4NjMtN2Q4ZGMyN2UyNmNhIWIyOTc3M3x4ZnNydC1zZXJ2aWNlLWJyb2tlciFiMzA5MSIsInVhYSJdfQ.qZfeDOcnYSyRTLedj2IECcAcksf2vER5BvsrINtlRikFBnXfOzYsnOUza8j-_hqa3s_T1usVcep6XClbdcDGJM5NNcdiSgRBwfbbTnH-O4hGveWJhM6oqWtolbKt3w30XgelfrRMO0M5Ymw_kVwIhdZ9qUjvFjOHfYQnPQFWf6au5mrIksLilf0p0E5J6BNlVEu9OrF7zxmE0qRGzDPw2GTLD56zHVp07DQh5YTp0HQLSerm7JnuddmVyjmJthtmuN1WDpdne29J6FC64JmYM3xZFdXSkMKS9ZL6rBvCEVhxZhZ3_5zMv1hvoQUoNj51EeZmSXPwTXV2Gc-0XZcJ8A";
let csrfToken;
let cookies;
async function _getDestination(destinationName) {
  const notifServiceDest = await getDestination(destinationName);

  if (!notifServiceDest) {
    throw new Error(`failed to get destination: ${destinationName}`);
  }
  return notifServiceDest;
}

class NotificationService {
  static async getNotificationTypes() {
    try {
      // const response = await executeHttpRequest(notifServiceDest, {
      //   url: `${notificationTypesEndpoint}/NotificationTypes`,
      //   method: "get"
      // });
      const notifServiceDest = await _getDestination(destinationName);
      const response = await axios.get("/v2/NotificationType.svc", {
        baseURL: "https://notifications.cfapps.us10.hana.ondemand.com",
        headers: {
          Authorization: `Bearer ${BearerToken}`,
          "X-CSRF-Token": "fetch"
        }
      });
      csrfToken = response.headers["x-csrf-token"];
      cookies = response.headers["set-cookie"];

      return response.data.d.EntitySets;
    } catch (error) {
      debugger;
    }
  }

  static async postNotificationType(notificationType) {
    try {
      const notifServiceDest = await _getDestination(destinationName);
      const csrfHeaders = await buildCsrfHeaders(notifServiceDest, { url: notificationTypesEndpoint });
      // const response = await executeHttpRequest(notifServiceDest, {
      // url: `${notificationTypesEndpoint}/NotificationTypes`,
      //   method: "post",
      //   data: notificationType,
      //   headers: csrfHeaders
      // });
      const response1 = await axios.get("/v2/NotificationType.svc/NotificationTypes?$expand=Templates,Actions", {
        baseURL: "https://notifications.cfapps.us10.hana.ondemand.com",
        headers: {
          Authorization: `Bearer ${BearerToken}`,

          Accept: "application/json"
        }
      });

      const response = await axios.post("/v2/NotificationType.svc/NotificationTypes", notificationType, {
        baseURL: "https://notifications.cfapps.us10.hana.ondemand.com",
        headers: {
          Authorization: `Bearer ${BearerToken}`,

          "X-CSRF-Token": csrfToken,
          Cookie: cookies,
          Accept: "application/json"
        }
      });
      // csrfToken = response.headers["x-csrf-token"];
      cookies = response.headers["set-cookie"];
      return response.data.d;
    } catch (error) {
      debugger;
    }
  }

  static async postNotification(notification) {
    try {
      const notifServiceDest = await _getDestination(destinationName);
      const csrfHeaders = await buildCsrfHeaders(notifServiceDest, { url: notificationEndpoint });
      // const response = await executeHttpRequest(notifServiceDest, {
      //   url: `${notificationEndpoint}/Notifications`,
      //   method: "post",
      //   data: notification,
      //   headers: csrfHeaders
      // });
      const response = await axios.post("/v2/Notification.svc/Notifications", notification, {
        baseURL: "https://notifications.cfapps.us10.hana.ondemand.com",
        headers: {
          Authorization: `Bearer ${BearerToken}`,

          "X-CSRF-Token": csrfToken,
          Cookie: cookies,
          Accept: "application/json"
        }
      });

      return response.data.d;
    } catch (error) {
      debugger;
    }
  }
}

module.exports = { NotificationService };
